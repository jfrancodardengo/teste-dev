(function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()

const campoCep = document.getElementById('inputCep')
const campoEndereco = document.getElementById('inputEndereco')
const campoCidade = document.getElementById('inputCidade')
const campoEstado = document.getElementById('inputEstado')

campoCep.addEventListener("blur", (event) => {
  const cep = event.target.value;
  fetch(`https://viacep.com.br/ws/${cep}/json/`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      campoEndereco.value = data.logradouro + ', ' + data.bairro;
      campoCidade.value = data.localidade;
      campoEstado.value = data.uf;
    })
})
